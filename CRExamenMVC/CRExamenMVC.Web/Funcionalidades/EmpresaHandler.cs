﻿using CRExamenMVC.Modelo;
using CRExamenMVC.Repositorio;
using CRExamenMVC.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRExamenMVC.Web.Funcionalidades
{
    [Authorize]
    public class EmpresaHandler : IDisposable
    {
        private readonly EmpresaRepositorio oEmpresaRepositorio;

        public EmpresaHandler()
        {
            oEmpresaRepositorio = new EmpresaRepositorio(new MVCContext());
        }

        public void Dispose()
        {
            oEmpresaRepositorio.Dispose();
        }

        public IEnumerable<EmpresaViewModels> ListarEmpresa()
        {
            var consulta = oEmpresaRepositorio.Empresas.TrerTodos();
            return consulta.Select(e => new EmpresaViewModels()
            {
                EmpresaID = e.EmpresaID,
                Departamento = e.Departamento,
                Direccion = e.Direccion,
                Distrito = e.Distrito,
                Provincia = e.Provincia,
                RazonSocial = e.RazonSocial,
                Rubro = e.Rubro,
                RUC = e.RUC,
                Usuario = e.Usuario
            }).ToList();

        }

        public EmpresaViewModels ConsultarEmpresaPorId(int id)
        {
            var consulta = oEmpresaRepositorio.Empresas.TraerUno(e => e.EmpresaID == id);
            return new EmpresaViewModels()
            {
                Departamento = consulta.Departamento,
                Direccion = consulta.Direccion,
                Distrito = consulta.Distrito,
                EmpresaID = consulta.EmpresaID,
                Provincia = consulta.Provincia,
                RazonSocial = consulta.RazonSocial,
                Rubro = consulta.Rubro,
                RUC = consulta.RUC
            };
            
        }

        public void RegistrarEmpresa(EmpresaViewModels oEmpresaViewModels)
        {
            oEmpresaRepositorio.Empresas.Agregar(new Empresa()
            {
                Departamento = oEmpresaViewModels.Departamento,
                Direccion = oEmpresaViewModels.Direccion,
                Distrito = oEmpresaViewModels.Distrito,
                Provincia = oEmpresaViewModels.Provincia,
                RazonSocial = oEmpresaViewModels.RazonSocial,
                Rubro = oEmpresaViewModels.Rubro,
                RUC = oEmpresaViewModels.RUC,
                Usuario = oEmpresaViewModels.Usuario
            });
            oEmpresaRepositorio.commit();
        }

        public void EditarEmpresa(EmpresaViewModels oEmpresaViewModels)
        {
            var oEmpresa = new Empresa()
            {
                Departamento = oEmpresaViewModels.Departamento,
                Direccion = oEmpresaViewModels.Direccion,
                Distrito = oEmpresaViewModels.Distrito,
                Provincia = oEmpresaViewModels.Provincia,
                RazonSocial = oEmpresaViewModels.RazonSocial,
                Rubro = oEmpresaViewModels.Rubro,
                RUC = oEmpresaViewModels.RUC,
                EmpresaID = oEmpresaViewModels.EmpresaID
            };

            oEmpresaRepositorio.Empresas.Actualizar(oEmpresa);
            oEmpresaRepositorio.commit();
        }

        public void EliminarEmpresa(int id)
        {
            var oEmpresa = oEmpresaRepositorio.Empresas.TraerUno(e => e.EmpresaID == id);
            oEmpresaRepositorio.Empresas.Eliminar(oEmpresa);
            oEmpresaRepositorio.commit();
        }
    }
}