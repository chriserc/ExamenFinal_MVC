﻿using CRExamenMVC.Web.Funcionalidades;
using CRExamenMVC.Web.Models;
using System.Web.Mvc;


namespace CRExamenMVC.Web.Controllers
{
    [Authorize]
    public class EmpresaController : Controller
    {
        
        [HttpGet]
        public ActionResult Lista()
        {
            using (var oEmpresaHandler = new EmpresaHandler())
            {
                return View(oEmpresaHandler.ListarEmpresa());
            }
        }


        [HttpGet]
        public ActionResult Registrar()
        {
            return View(new EmpresaViewModels());
        }
        [HttpPost]
        public ActionResult Registrar(EmpresaViewModels oEmpresaViewModels)
        {
            if (!ModelState.IsValid) return View(oEmpresaViewModels);
            using (var oEmpresaHandler = new EmpresaHandler())
            {
                try
                {
                    oEmpresaHandler.RegistrarEmpresa(oEmpresaViewModels);
                    return RedirectToAction("Lista");
                }
                catch (System.Exception)
                {
                    return View(oEmpresaViewModels);
                }
            }
        }


        [HttpGet]
        public ActionResult Details(int id)
        {
            using (var oEmpresaHandler = new EmpresaHandler())
            {
                return View(oEmpresaHandler.ConsultarEmpresaPorId(id));
            }
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var oEmpresaHandler = new EmpresaHandler())
            {
                return View(oEmpresaHandler.ConsultarEmpresaPorId(id));
            }
        }
        [HttpPost]
        public ActionResult Edit(EmpresaViewModels oEmpresaViewModels)
        {
            if (!ModelState.IsValid) return View(oEmpresaViewModels);
            using (var oEmpresaHandler = new EmpresaHandler())
            {
                try
                {
                    oEmpresaHandler.EditarEmpresa(oEmpresaViewModels);
                    return RedirectToAction("Lista");
                }
                catch (System.Exception)
                {
                    return View(oEmpresaViewModels);
                }
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            using (var oEmpresaHandler = new EmpresaHandler())
            {
                return View(oEmpresaHandler.ConsultarEmpresaPorId(id));
            }
        }
        [HttpPost]
        public ActionResult Delete(int id, EmpresaViewModels oEmpresaViewModels)
        {
            using (var oEmpresaHandler = new EmpresaHandler())
            {
                try
                {
                    oEmpresaHandler.EliminarEmpresa(id);
                    return RedirectToAction("Lista");
                }
                catch (System.Exception)
                {
                    return View(oEmpresaViewModels);
                }
            }
        }
    }
}
