﻿using FluentValidation.Attributes;

namespace CRExamenMVC.Web.Models
{
    [Validator(typeof(EmpresaValidator))]
    public class EmpresaViewModels
    {
        public int EmpresaID { get; set; }
        public string Usuario { get; set; }
        public string RUC { get; set; }
        public string RazonSocial { get; set; }
        public string Direccion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Rubro { get; set; }
    }
}