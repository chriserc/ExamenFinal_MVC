﻿using FluentValidation;

namespace CRExamenMVC.Web.Models
{
    public class EmpresaValidator : AbstractValidator<EmpresaViewModels>
    {
        public EmpresaValidator()
        {

            RuleFor(e => e.RUC).NotEmpty()
                .Length(11).WithMessage("Debe tener 11 digitos");

            RuleFor(e => e.RazonSocial).NotEmpty();
        }
    }
}