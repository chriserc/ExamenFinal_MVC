﻿using System;

namespace CRExamenMVC.Web.Models
{
    public class FacturaViewModels
    {
        public int FacturasID { get; set; }

        public int? EmpresaID { get; set; }

        public int? Numero { get; set; }


        public DateTime? FechaEmision { get; set; }


        public DateTime? FechaVenc { get; set; }


        public DateTime? FechaCobro { get; set; }

        public int? Ruc { get; set; }

        public string RazonSocial { get; set; }

        public decimal? TotalFactura { get; set; }

        public decimal? TotalImp { get; set; }

        public string FacturaEscaneada { get; set; }

    }
}