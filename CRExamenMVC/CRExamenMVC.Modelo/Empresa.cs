namespace CRExamenMVC.Modelo
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Empresa")]
    public partial class Empresa
    {
        public int EmpresaID { get; set; }

        [StringLength(100)]
        public string Usuario { get; set; }

        [StringLength(11)]
        public string RUC { get; set; }

        [StringLength(100)]
        public string RazonSocial { get; set; }

        [StringLength(100)]
        public string Direccion { get; set; }

        [StringLength(50)]
        public string Departamento { get; set; }

        [StringLength(50)]
        public string Provincia { get; set; }

        [StringLength(50)]
        public string Distrito { get; set; }

        [StringLength(100)]
        public string Rubro { get; set; }
    }
}
