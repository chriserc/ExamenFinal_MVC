namespace CRExamenMVC.Modelo
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Facturas
    {
        public int FacturasID { get; set; }

        public int? EmpresaID { get; set; }

        public int? Numero { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaEmision { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaVenc { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaCobro { get; set; }

        public int? Ruc { get; set; }

        [StringLength(100)]
        public string RazonSocial { get; set; }

        public decimal? TotalFactura { get; set; }

        public decimal? TotalImp { get; set; }

        [StringLength(100)]
        public string FacturaEscaneada { get; set; }
    }
}
