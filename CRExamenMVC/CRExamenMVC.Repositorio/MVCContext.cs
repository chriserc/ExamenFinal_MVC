namespace CRExamenMVC.Repositorio
{
    using Modelo;
    using System.Data.Entity;

    public partial class MVCContext : DbContext
    {
        public MVCContext()
            : base("name=MVCContext")
        {
        }

        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<Facturas> Facturas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Empresa>()
                .Property(e => e.Usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.RazonSocial)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Departamento)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Provincia)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Distrito)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Rubro)
                .IsUnicode(false);

            modelBuilder.Entity<Facturas>()
                .Property(e => e.RazonSocial)
                .IsUnicode(false);

            modelBuilder.Entity<Facturas>()
                .Property(e => e.TotalFactura)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Facturas>()
                .Property(e => e.TotalImp)
                .HasPrecision(12, 2);

            modelBuilder.Entity<Facturas>()
                .Property(e => e.FacturaEscaneada)
                .IsUnicode(false);
        }
    }
}
