﻿using CRExamenMVC.Modelo;
using System;
using System.Data.Entity;

namespace CRExamenMVC.Repositorio
{
    public class EmpresaRepositorio : IDisposable
    {
        private readonly DbContext db;
        private readonly RepositorioGenerico<Empresa> _empresas;
        public RepositorioGenerico<Empresa> Empresas {
            get { return _empresas; }
        }

        public EmpresaRepositorio(DbContext db)
        {
            this.db = db;
            _empresas = new RepositorioGenerico<Empresa>(db);
        }

        public void commit()
        {
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
