﻿using CRExamenMVC.Modelo;
using System;
using System.Data.Entity;


namespace CRExamenMVC.Repositorio
{
    public class FacturaRepositorio : IDisposable 
    {
        private readonly DbContext db;
        private readonly RepositorioGenerico<Facturas> _facturas;
        public RepositorioGenerico<Facturas> Facturas
        {
            get { return _facturas; }
        }

        public FacturaRepositorio(DbContext db)
        {
            this.db = db;
            _facturas = new RepositorioGenerico<Facturas>(db);
        }

        public void commit()
        {
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
